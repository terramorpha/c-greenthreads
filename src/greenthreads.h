#ifndef GREENTHREADS_H
#define GREENTHREADS_H
#include <ucontext.h>


typedef struct Task {
    ucontext_t env;
    struct Scheduler * scheduler;
    int done;
} Task;

typedef struct Scheduler {
    ucontext_t * run;
    Queue tasks;
    int epoll_fd;
    int epoll_pending;
} Scheduler;

int green_read(int fd, void * buf, int len);
int green_prepare_fd(int fd);
void green_sleep(int sec);

void green_sched_init(Scheduler*sched);
void green_sched_deinit(Scheduler*sched);
void green_yield();
void green_run(Scheduler * tq);
void green_spawn(Scheduler * tq, void (*proc)(Scheduler *, void *), void *);

#endif
