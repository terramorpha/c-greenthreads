#ifndef QUEUE_H
#define QUEUE_H

typedef struct Queue {

    void ** buffer;
    int cap;
    int tail;
    int head;

} Queue;

void queue_init(Queue * this, int cap);
void queue_deinit(Queue * this);
void queue_push(Queue * this, void * val);
void * queue_pop(Queue * this);
int queue_length(Queue * this);

#endif
