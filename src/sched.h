#ifndef SCHED_H
#define SCHED_H
#include <ucontext.h>
#include "queue.h"
#include "greenthreads.h"

Task * scheduler_current_task();
void scheduler_init(Scheduler * tq);
void scheduler_deinit(Scheduler * sched);
Task * scheduler_push(Scheduler * tq, Task t);
Task scheduler_pop(Scheduler * tq);
void scheduler_spawn(Scheduler * tq, void (*proc)(Scheduler *, void *), void *);
void scheduler_yield();
void scheduler_yield_io(int fd);
void scheduler_run(Scheduler * tq);
void scheduler_suspend();
void scheduler_reschedule(Scheduler * sched, Task * t);
#endif
