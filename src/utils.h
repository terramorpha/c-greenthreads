#ifndef UTILS_H
#define UTILS_H
#include <errno.h>
#include <error.h>

#define err(msg)                                                        \
    do {                                                                \
        error_at_line(1, errno, __FILE__, __LINE__, msg); \
    } while (0)

#endif
