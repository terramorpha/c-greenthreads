#include <stdlib.h>
#include "queue.h"

int queue_length(Queue * this) {
    return this->tail - this->head;
}

void queue_init(Queue * this, int cap) {
    this->buffer = malloc(sizeof(void*) * cap);
    this->cap = cap;
    this->tail = 0;
    this->head = 0;
}

void queue_deinit(Queue * this) {
    free(this->buffer);
}

void queue_resize(Queue * this) {

    int new_size = this->cap * 2;

    void ** new_buf = malloc(sizeof(void*) * new_size);

    int n = queue_length(this);
    for (int i = 0;i < n;i++) {
        new_buf[i] = this->buffer[(this->head + i) % this->cap];
    }

    free(this->buffer);
    this->head = 0;
    this->tail = n;
    this->cap = new_size;
    this->buffer = new_buf;
}

void queue_push(Queue * this, void * val) {
    if (queue_length(this) == this->cap)
        queue_resize(this);
    this->buffer[this->tail++ % this->cap] = val;
}

void * queue_pop(Queue * this) {
    return this->buffer[this->head++ % this->cap];
}

