#include "queue.h"
#include "sched.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <ucontext.h>
#include <unistd.h>

static Task * current_task;

Task * scheduler_current_task() {
    return current_task;
}

void scheduler_init(Scheduler * tq) {
    tq->run = NULL;
    queue_init(&tq->tasks, 50);

    tq->epoll_fd = epoll_create(1);
    tq->epoll_pending = 0;
}

void scheduler_deinit(Scheduler * sched) {
    close(sched->epoll_fd);
    queue_deinit(&sched->tasks);
}

// Est utilisé pour que lorsque la fonction termine, elle yield au scheduler.
// Il faudrait aussi free() la stack associée
static
void
task_wrapper(Scheduler * tq, void (*proc)(Scheduler *, void *), void * data) {
    proc(tq, data);
    Task * task = current_task;
    task->done = 1;
    setcontext(tq->run);
}

void
scheduler_spawn
(Scheduler * tq, void (*proc)(Scheduler *, void * data), void * data) {

    if (tq == NULL) {
        Task * ct = scheduler_current_task();
        tq = ct->scheduler;
    }

    Task * nt = malloc(sizeof(*nt));
    nt->scheduler = tq;
    getcontext(&nt->env);
    int stack_size = 1 << 12;
    void * ns = malloc(stack_size);
    nt->env.uc_stack.ss_size = stack_size;
    nt->env.uc_stack.ss_sp = ns;
    nt->done = 0;
    makecontext(&nt->env, (void (*)(void))task_wrapper, 3, tq, proc, data);
    queue_push(&tq->tasks, nt);
}



// Suspend the task while having it rescheduled
void scheduler_yield() {
    Task *tp = scheduler_current_task();
    Scheduler * tq = tp->scheduler;
    queue_push(&tq->tasks, tp);
    swapcontext(&tp->env, tq->run);
}

// Suspend the task while only updating the context: do not reschedule it.
void scheduler_suspend() {
    Task * t = scheduler_current_task();
    swapcontext(&t->env, t->scheduler->run);
}

struct io_data {
    Task * task;
    int fd;
};

void scheduler_yield_io(int fd) {
    Task *tp = scheduler_current_task();
    Scheduler * tq = tp->scheduler;

    struct epoll_event event;

    struct io_data data; // = malloc(sizeof(*data));

    data.fd = fd;
    data.task = tp;
    event.data.ptr = &data;
    event.events = EPOLLIN;
    int res = epoll_ctl(tq->epoll_fd, EPOLL_CTL_ADD, fd, &event);

    tq->epoll_pending++;
    if (res < 0)
        err("adding file descriptor");

    swapcontext(&tp->env, tq->run);
}

int scheduler_empty(Scheduler * sched) {
    return sched->tasks.head == sched->tasks.tail;
}


void scheduler_run(Scheduler * tq) {
    ucontext_t ctx;
    tq->run = &ctx;
    while (1) {
        while (!scheduler_empty(tq)) {
            Task * t = queue_pop(&tq->tasks);
            current_task = t;
            swapcontext(tq->run, &t->env);

            if (t->done) {
                free(t->env.uc_stack.ss_sp);
                free(t);
            }
        }

        if (tq->epoll_pending == 0) {
            return;
        }
        
        int n = 5;
        struct epoll_event events[n];
         // attend indéfiniment
        int res = epoll_wait(tq->epoll_fd, events, n, -1);
        
        if (res < 0)
            err("waiting for epoll");

        tq->epoll_pending -= res;

        for (int i = 0;i < res;i++) {

            struct io_data * data = events[i].data.ptr;

            int fd = data->fd;
            queue_push(&tq->tasks, data->task);
            //free(data);

            epoll_ctl(tq->epoll_fd, EPOLL_CTL_DEL, fd, NULL);
        }
    }
}

void scheduler_reschedule(Scheduler * sched, Task * t) {
    if (sched == NULL) {
        sched = scheduler_current_task()->scheduler;
    }

    queue_push(&sched->tasks, t);
}
