#include <sys/socket.h>
#include <netinet/in.h>
#include "utils.h"
#include <stdio.h>
#include "async.h"
#include <unistd.h>

void test(void * _, int port) {

    int res;
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0)
        err("creating socket");

    struct sockaddr_in myaddr;

    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(0);
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    res = bind(fd, (struct sockaddr *)&myaddr, sizeof(myaddr));
    if (res < 0)
        err("binding socket");

    printf("worked\n");


    struct sockaddr_in raddr;
    raddr.sin_family = AF_INET;
    raddr.sin_port = htons(port);
    raddr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    res = connect(fd, (struct sockaddr*)&raddr, sizeof(myaddr));

    if (res < 0)
        err("connecting");

    int len = 1 << 5;
    char buf[len];

    async_make(fd);

    while (1) {
        res = async_read(fd , buf, len);
        if (res < 0)
            err("reading");
        else if (res == 0)
            break;

        int written = 0;
        while (written < res) {
            int w = write(1, buf, res);
            written += w;
        }
    }
}
