#ifndef CHANNEL_H
#define CHANNEL_H
#include "queue.h"
#include "sched.h"

typedef struct Channel {
    Queue read_queue;
    Queue write_queue;
} Channel;

void channel_init(Channel * chan);
void * channel_read(Channel * chan);
void channel_write(Channel * chan, void * data);

#endif
