#include "async.h"
#include "sched.h"
#include "utils.h"
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/timerfd.h>
#include <unistd.h>

int async_read(int fd, void *buf, int len) {
    int res;

 try : res
        = read(fd, buf, len);

    if (res >= 0)
        return res;

    if (errno != EAGAIN)
        return res;

    scheduler_yield_io(fd);
    goto try
        ;
}

int async_make(int fd) {
    int flags = fcntl(fd, F_GETFL);
    flags |= O_NONBLOCK;
    return fcntl(fd, F_SETFL, flags);
}

void async_sleep(int sec) {
    int res;
    int timerfd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);

    struct itimerspec utmr;
    uint64_t num;
    utmr.it_value.tv_sec = 1;
    utmr.it_value.tv_nsec = 0;

    // on répète pas
    utmr.it_interval.tv_sec = 0;
    utmr.it_interval.tv_nsec = 0;
    timerfd_settime(timerfd, 0, &utmr, NULL);

    res = async_read(timerfd, &num, sizeof(num));
    if (res < 0)
        err("async read'ing");
    close(timerfd);
}
