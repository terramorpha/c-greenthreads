#include "channel.h"
#include "sched.h"
#include <stddef.h>

void channel_init(Channel * chan) {
    queue_init(&chan->read_queue, 1);
    queue_init(&chan->write_queue, 1);
}

struct channel_entry_data {
    Task * task;
    void * data;
};

void channel_write(Channel * chan, void * sdata) {

    Task * ct = scheduler_current_task();
    if (queue_length(&chan->read_queue) == 0) {
        struct channel_entry_data data;
        data.task = ct;
        data.data = sdata;
        queue_push(&chan->write_queue, &data);
        scheduler_suspend();
    } else {
        struct channel_entry_data * data = queue_pop(&chan->read_queue);
        *(void**)data->data = sdata;
        scheduler_reschedule(NULL, data->task);
    }
}

void * channel_read(Channel * chan) {
    if (queue_length(&chan->write_queue) == 0) {
        // se suspend
        void * dataptr;
        struct channel_entry_data data;
        data.task = scheduler_current_task();
        data.data = &dataptr;
        queue_push(&chan->read_queue, &data);
        scheduler_suspend();
        return dataptr;
    } else {
        struct channel_entry_data * data = queue_pop(&chan->write_queue);
        void * result = data->data;
        scheduler_reschedule(NULL, data->task);
        return result;
    }
    return NULL;
}
